using NUnit.Framework;
using TRPO_Lab3.Lib;
namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            double a = 3;
            double b = 4;
            double c = 5;
            double s = 10;
            double expected = 45.82;

            var actual = Formules.TreyG(s, a, b, c);
            Assert.AreEqual(expected, actual, 0.01d);
        }
    }
}