﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Formules
    {
        public static double TreyG(double s, double a, double b, double c)
        {
            double TreyG = Math.Sqrt(s*(s - a) * (s - b) * (s - c));
            return TreyG;
        }
    }
}
